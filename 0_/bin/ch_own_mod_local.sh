#!/bin/bash
# /var/www/html/laravelearn/0_bin/ch_own_mod_local.sh
verz=/var/www/html/laravelearn/
cd $verz
chmodFolder=775
chmodFile=664
chown -R allapow:www-data .
find . -type d -exec chmod $chmodFolder {} \;
find . -type f -exec chmod $chmodFile {} \;

chmod -R 775 storage