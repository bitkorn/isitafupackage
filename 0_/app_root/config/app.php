<?php

return [


    'providers' => [

        /*
         * 
         */
        Bitkorn\IsitafuPackage\Providers\IsitafuPackageServiceProvider::class
    ],


    'aliases' => [
        /**
         * to use it e.g. in views: tools::printrTextarea($user)
         * Only static functions allowed!
         */
        'tools' => Bitkorn\IsitafuPackage\Helpers\Tools::class,

    ],

];
