<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        /*
         * Global Middleware for all routes
         */
        \Bitkorn\IsitafuPackage\Http\Middleware\CheckApiAccess::class
    ];

}
