<?php

use Illuminate\Support\Facades\Route;

Route::get('/test', function () {
    return 'Test success :)';
})->name('test');

Route::get('/testview', function () {
    return view('bitkorn/isitafu::test');
})->name('testview');

Route::get('/testviewohneblade', function () {
    return view('bitkorn/isitafu::testohneblade');
})->name('testviewohneblade');

/**
 *
 */
Route::get('user/{userid}', 'Bitkorn\IsitafuPackage\Http\Controllers\IsitafuController@user')->name('userid')->middleware('checkApiAccess');
