<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/w3school.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
    <script type="text/javascript" src="/js/vendor/vue.js"></script>
    <script type="text/javascript" src="/js/vendor/jquery.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>
    <title>Laravel - @yield('title')</title>

</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2 w3-light-gray">
            <div class="w3-sidebar w3-bar-block w3-sand">
                @section('sidebar')
                    <a href="<?php echo url('test'); ?>" class="w3-bar-item w3-button">Test</a>
                    <a href="{{ url('testview') }}" class="w3-bar-item w3-button">TestView</a>
                    <a href="{{ url('testviewohneblade') }}" class="w3-bar-item w3-button">TestView ohne Blade</a>
                    <a href="{{ url('user', ['userid' => 1]) }}" class="w3-bar-item w3-button" title="With middleware">User 1</a>
                @show
            </div>
            $nbsp;
        </div>
        <div class="col-sm-10 w3-gray">
            @yield('content')
        </div>
    </div>
</div>
</body>
</html>
