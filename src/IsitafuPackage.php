<?php

namespace Bitkorn\IsitafuPackage;


class IsitafuPackage
{
    /**
     * Multiplies the two given numbers
     * @param int $a
     * @param int $b
     * @return int
     */
    public function multiply($a, $b)
    {
        return $a * $b;
    }
}
