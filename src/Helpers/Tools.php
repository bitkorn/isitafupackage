<?php

namespace Bitkorn\IsitafuPackage\Helpers;

/**
 *
 * @author Torsten Brieskorn
 */
class Tools
{
    public static function printrTextarea($printrContent = null)
    {
        if(isset($printrContent) && $printrContent) {
            return '<textarea cols="50" rows="10">' . print_r($printrContent, true) . '</textarea>';
        }
    }

    /**
     *
     *
     * @return string
     * @throws ErrorException (E_DEPRECATED)
     * Non-static method Bitkorn\IsitafuPackage\Helpers\Tools::getSomething() should not be called statically
     */
    public function getSomething()
    {
        return 'Something from ' . __CLASS__ . '()->' . __FUNCTION__;
    }
}
