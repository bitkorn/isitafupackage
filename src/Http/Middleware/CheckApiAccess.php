<?php

namespace Bitkorn\IsitafuPackage\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class CheckApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        Log::info('Middleware');
        return $next($request);
    }
}
