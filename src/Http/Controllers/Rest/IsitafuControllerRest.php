<?php
/**
 * https://laravel.com/docs/5.7/controllers#resource-controllers
 */

namespace Bitkorn\IsitafuPackage\Http\Controllers\Rest;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * http://laravel.local/api/isitafu
 *
 * Class IsitafuControllerRest
 * @package Bitkorn\IsitafuPackage\Http\Controllers\Rest
 */
class IsitafuControllerRest extends Controller
{
    /**
     * GET
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info(__FUNCTION__);
    }

    /**
     * POST
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info(__FUNCTION__);
    }

    /**
     * GET with route parameter
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info(__FUNCTION__);
    }

    /**
     * PUT/PATCH
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info(__FUNCTION__);
    }

    /**
     * DELETE
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info(__FUNCTION__);
    }
}
