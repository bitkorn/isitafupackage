<?php

namespace Bitkorn\IsitafuPackage\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IsitafuController extends Controller
{
    
    /**
     * http://laravel.local/user/1
     * 
     * @param int $userid
     * @return type
     */
    public function user(int $userid)
    {
        $user = DB::select('SELECT * FROM user WHERE user_id=:user_id', ['user_id' => $userid]);
        
        return view('bitkorn/isitafu::deeper.user', ['user' => $user]);
    }
}
