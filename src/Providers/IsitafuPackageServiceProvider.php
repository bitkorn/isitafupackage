<?php

namespace Bitkorn\IsitafuPackage\Providers;

use Bitkorn\IsitafuPackage\IsitafuPackage;
use Illuminate\Support\ServiceProvider;

class IsitafuPackageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @param \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        /**
         * Overwrite routes:
         * In config/app.php in the providers array put the service provider of the package before App\Providers\RouteServiceProvider::class
         */
        require_once __DIR__ . '/../../routes/web.php';
        require_once __DIR__ . '/../../routes/api.php';

        $this->loadViewsFrom(__DIR__ . '/../../views/', 'bitkorn/isitafu');

        /*
         * for use in routes
         */
        $router->aliasMiddleware('checkApiAccess', \Bitkorn\IsitafuPackage\Http\Middleware\CheckApiAccess::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IsitafuPackage::class, function () {
            return new IsitafuPackage();
        });
        /*
         * to use it e.g. in routes (e.g.: bitkorn/isitafu::test)
         */
        $this->app->alias(IsitafuPackage::class, 'isitafu');

        require_once __DIR__ . '/../Helpers/Tools.php';
    }

}
