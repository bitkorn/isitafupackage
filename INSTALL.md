Create folder **/packages**

Im Terminal

    git clone https://bitbucket.org/bitkorn/isitafupackage.git

In /composer.json

    "repositories": [
        {
            "type": "path",
            "url": "./packages/Bitkorn/IsitafuPackage",
            "options": {
                "symlink": true
            }
        }
    ],
    "require": {
        "bitkorn/IsitafuPackage": "dev-master"
    },

...dann im Terminal

    composer update

In /config/app.php

    return [
        'providers' => [
            Bitkorn\IsitafuPackage\Providers\IsitafuPackageServiceProvider::class
        ],
    ];